# SRE-exercise-application

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mkumar16888/sre-exercise-application.git
git branch -M main
git push -uf origin main
```

***

## Sample App - Docker Image and Helm Chart

## Description
This repository contains the Helm chart for deploying the Sample App and a Dockerfile for building the Docker image.

## Docker Image
The Dockerfile for building the Sample App Docker image is located at the root of this repository. Follow the steps below to build the Docker image:

## Prerequisites
Docker is installed on your local machine. if not can be downloaded from [Download_Docker](https://docs.docker.com/engine/install/)

## Building the Docker Image
1. Clone this repository to your local machine (if you haven't already).

2. Change directory to the root folder of the repository.

3. Build the Docker image. run the following command:
    
    **docker build -t sample-app:latest .**


## Helm Chart

The Helm chart for the Sample App is located in the `sample-app/helm-chart` folder. The chart is used to deploy the Sample App to a Kubernetes cluster. Follow the steps below to deploy the app using Helm:

### Prerequisites

- Kubernetes cluster is up and running.
  IAAC for spinning up the kubernetes cluster in AWS can be referred to the following repository:- [IAAC-AWS](https://gitlab.com/mkumar16888/sre-exercise)
- Helm is installed on your local machine and properly configured to interact with the cluster. if not helm can be downloaded from the following link: [Install Helm](https://helm.sh/docs/intro/install/)

### Deployment

1. Clone this repository to your local machine.

2. Change directory to the Helm chart folder:

   **cd sample-app/helm-chart**

3. Customize the values.yaml file to configure the deployment settings. Modify any configuration parameters as needed for your environment.

4. Install the Helm chart:
   
   **helm install sample-app .**

5. Verify that the deployment is successful:

   **kubectl get pods**

6. Access the Sample App using the exposed service URL.
